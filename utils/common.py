import subprocess
from utils.errors import InvalidURLError

def get_repo_name_from_url(url):
    """
    Returns the repository name from url

    :param url: remote repo url
    :type url: str
    """
    last_slash_index = url.rfind("/")
    last_suffix_index = url.rfind(".git")
    if last_suffix_index < 0:
        last_suffix_index = len(url)

    if last_slash_index < 0 or last_suffix_index <= last_slash_index:
        raise InvalidURLError

    return url[last_slash_index + 1:last_suffix_index]

def exec_command(command):
    """
    Executes the program from the shell
    :param command: shell command to execute
    :type command: str
    """
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    return output.decode('UTF-8')