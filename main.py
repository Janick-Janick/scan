import os
import sys
import argparse
import logging
from application.app import Application

logging.basicConfig(level=logging.WARNING)

def main():
    parser = argparse.ArgumentParser(prog='Python Application')
    parser.add_argument('--remote-git-repo', nargs='?', help='URL to remote git repo')
    parser.add_argument('--git-commit-ref', nargs='?', help='git commit reference that should be executed')
    parser.add_argument('--param', nargs='?', help='parameter "x" to be processed')

    args = parser.parse_args()
    Application(**vars(args)).run()

if __name__ == '__main__':
    main()

