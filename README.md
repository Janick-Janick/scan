This application clones remote git repo to your local folder and executes it.

To run the application execute the following commands:
1. virtualenv -p python3 .venv
2. . .venv/bin/activate
3. pip3 install -r requirements.txt
4. python main.py --remote-git-repo *remote repo url* --git-commit-ref *git commit reference* --param *your X param value*



