class ErrorMessage(Exception):
    MESSAGE = None

    def __init__(self, message=None):
        if not message:
            message = self.MESSAGE
        super(ErrorMessage, self).__init__(self, message)

class InvalidURLError(ErrorMessage):
    MESSAGE = 'Badly formatted url'