import os
import subprocess
from git.repo.base import Repo
import git

class GitClient():
    """
    Simple class for gitpython lib interraction
    """

    def __init__(self, repo_url=None, commit_ref=None, repo_name=None):
        self.repo_url = repo_url
        self.commit_ref = commit_ref
        self.repo_name = repo_name

    def git_clone(self):
        repo = Repo.clone_from(self.repo_url, self.repo_name)
    
    def git_checkout(self):
        cdir = os.getcwd()
        repo = git.Repo(os.path.join(cdir, self.repo_name))
        commit = repo.git.checkout(self.commit_ref)
   
