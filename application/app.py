import os
import sys
import shutil
import logging
from utils.git import GitClient
from utils.common import get_repo_name_from_url, exec_command


class Application():
    
    def __init__(self, remote_git_repo=None, git_commit_ref=None, param=None):
        if not remote_git_repo:
            raise ValueError('provide --remote-git-repo')
        if not git_commit_ref:
            logging.warning('provide --git-commit-ref')
        if not param:
            raise ValueError('Warningprovide --param')
        self.param = param
        self.script_name = 'script.py'
        self.remote_git_repo = remote_git_repo
        self.repo_name = get_repo_name_from_url(self.remote_git_repo)
        self.git_commit_ref = git_commit_ref
        self.git_client = GitClient(repo_url=self.remote_git_repo, commit_ref=self.git_commit_ref, repo_name=self.repo_name)

    def run_script(self):
        try:
            # exec(open(os.path.join(self.repo_name, self.script_name)).read())
            command = 'python {0} --param {1}'.format(os.path.join(self.repo_name, self.script_name), self.param)
            result = exec_command(command)
            print(result)
        except FileNotFoundError:
            raise
        
    def remove_local_repo(self):
        shutil.rmtree(self.repo_name)

    def run(self):
        if not os.path.exists(self.repo_name):
            self.git_client.git_clone()
        if self.git_commit_ref:
            self.git_client.git_checkout()
        self.run_script()
        self.remove_local_repo()